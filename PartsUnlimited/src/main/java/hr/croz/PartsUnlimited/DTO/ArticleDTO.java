package hr.croz.PartsUnlimited.DTO;

public class ArticleDTO {

    private Long serialNumber;
    private Double basePrice;

    public ArticleDTO() {}

    public ArticleDTO(Long serialNumber, Double basePrice) {
        this.serialNumber = serialNumber;
        this.basePrice = basePrice;
    }

    public Long getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(Long serialNumber) {
        this.serialNumber = serialNumber;
    }

    public Double getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(Double basePrice) {
        this.basePrice = basePrice;
    }
}
