package hr.croz.PartsUnlimited.DTO;

public class BrandDTO {

    private String brandName;

    public BrandDTO(){}

    public BrandDTO(String brandName) {
        this.brandName = brandName;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }
}
