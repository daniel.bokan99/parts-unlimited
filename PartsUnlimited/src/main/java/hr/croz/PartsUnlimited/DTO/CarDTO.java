package hr.croz.PartsUnlimited.DTO;

public class CarDTO {

    private Long id;
    private String carName;
    private Long brandFK;

    public CarDTO(){}

    public CarDTO(Long id, String carName, Long brandFK) {
        this.id = id;
        this.carName = carName;
        this.brandFK = brandFK;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCarName() {
        return carName;
    }

    public void setCarName(String carName) {
        this.carName = carName;
    }

    public Long getBrandFK() {
        return brandFK;
    }

    public void setBrandFK(Long brandFK) {
        this.brandFK = brandFK;
    }
}
