package hr.croz.PartsUnlimited.DTO;

public class DateDTO {

    private String dateOfManufacture;

    public DateDTO(){}

    public DateDTO(String dateOfManufacture) {
        this.dateOfManufacture = dateOfManufacture;
    }

    public String getDateOfManufacture() {
        return dateOfManufacture;
    }

    public void setDateOfManufacture(String dateOfManufacture) {
        this.dateOfManufacture = dateOfManufacture;
    }
}
