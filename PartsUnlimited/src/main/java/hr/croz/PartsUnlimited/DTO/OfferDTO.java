package hr.croz.PartsUnlimited.DTO;

import java.time.LocalDate;

public class OfferDTO {

    private Long serialNumber;
    private LocalDate dateOfManufacture;
    private Double finalPrice;

    public OfferDTO() {}

    public OfferDTO(Long serialNumber, LocalDate dateOfManufacture, Double finalPrice) {
        this.serialNumber = serialNumber;
        this.dateOfManufacture = dateOfManufacture;
        this.finalPrice = finalPrice;
    }

    public Long getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(Long serialNumber) {
        this.serialNumber = serialNumber;
    }

    public LocalDate getDateOfManufacture() {
        return dateOfManufacture;
    }

    public void setDateOfManufacture(LocalDate dateOfManufacture) {
        this.dateOfManufacture = dateOfManufacture;
    }

    public Double getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(Double finalPrice) {
        this.finalPrice = finalPrice;
    }
}
