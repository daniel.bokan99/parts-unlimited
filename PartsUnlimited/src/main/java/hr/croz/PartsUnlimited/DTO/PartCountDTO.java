package hr.croz.PartsUnlimited.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

public class PartCountDTO {

    @JsonProperty("brand_and_automobile")
    private String brandAndCar;

    private int count;

    public PartCountDTO(){}

    public PartCountDTO(String brandAndCar, int count) {
        this.brandAndCar = brandAndCar;
        this.count = count;
    }

    public String getBrandAndCar() {
        return brandAndCar;
    }

    public void setBrandAndCar(String brandAndCar) {
        this.brandAndCar = brandAndCar;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
