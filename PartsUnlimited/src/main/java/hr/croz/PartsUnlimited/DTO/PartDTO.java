package hr.croz.PartsUnlimited.DTO;

import java.time.LocalDate;

public class PartDTO {

    private Long serialNumber;
    private LocalDate dateOfManufacture;
    private String partName;
    private Long carFK;

    public PartDTO(){}

    public PartDTO(Long serialNumber, LocalDate dateOfManufacture, String partName, Long carFK) {
        this.serialNumber = serialNumber;
        this.dateOfManufacture = dateOfManufacture;
        this.partName = partName;
        this.carFK = carFK;
    }

    public Long getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(Long serialNumber) {
        this.serialNumber = serialNumber;
    }

    public LocalDate getDateOfManufacture() {
        return dateOfManufacture;
    }

    public void setDateOfManufacture(LocalDate dateOfManufacture) {
        this.dateOfManufacture = dateOfManufacture;
    }

    public String getPartName() {
        return partName;
    }

    public void setPartName(String partName) {
        this.partName = partName;
    }

    public Long getCarFK() {
        return carFK;
    }

    public void setCarFK(Long carFK) {
        this.carFK = carFK;
    }
}
