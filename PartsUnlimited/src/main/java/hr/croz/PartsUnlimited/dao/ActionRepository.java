package hr.croz.PartsUnlimited.dao;

import hr.croz.PartsUnlimited.domain.Action;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.time.LocalDate;

public interface ActionRepository extends JpaRepository<Action, Long> {

    @Transactional
    @Query(
            value = "INSERT INTO Action(action_id,discount_percentage, end_date, start_date) VALUES(nextval('action_action_id_seq'),?1, ?2, ?3)  returning action_id",
                  nativeQuery = true
    )
    int addAction(Double discountPercentage, LocalDate endDate, LocalDate startDate);

    @Transactional
    @Query(
            value = "SELECT * FROM Action WHERE action_id = ?1",
                  nativeQuery = true
    )
    Action findByActionId(Long id);

    @Transactional
    @Modifying
    @Query(
            value = "INSERT INTO action_articles(action_action_id, articles_serial_number) VALUES(?1, ?2)",
                  nativeQuery = true
    )
    void addArticleToAction(Long actionID, Long articleSerialNumber);

    @Transactional
    @Modifying
    @Query(
            value = "UPDATE article SET action_action_id = ?1 WHERE serial_number = ?2",
                  nativeQuery = true
    )
    void addActionToArticle(Long actionID, Long articleSerialNumber);

}
