package hr.croz.PartsUnlimited.dao;

import hr.croz.PartsUnlimited.domain.Article;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;
import java.util.List;

public interface ArticleRepository extends JpaRepository<Article, Long> {

    @Modifying
    @Transactional
    @Query(
            value = "INSERT INTO article(serial_number, base_price) VALUES (?1, ?2)",
            nativeQuery = true
    )
    void addArticle(Long serialNumber, Double basePrice);

    @Transactional
    @Query(
            value = "SELECT * FROM Article WHERE serial_number = ?1",
            nativeQuery = true
    )
    Article findBySerialNumber(Long serialNumber);

    @Modifying
    @Transactional
    @Query(
            value = "UPDATE Article SET base_price = ?2 WHERE serial_number = ?1",
            nativeQuery = true
    )
    void updateBasePrice(Long serialNumber, Long basePrice);

    @Modifying
    @Transactional
    @Query(
            value = "DELETE FROM article WHERE serial_number = ?1",
            nativeQuery = true
    )
    void deleteArticle(Long serialNumber);

    @Modifying
    @Transactional
    @Query(
            value = "DELETE FROM action_articles WHERE articles_serial_number = ?1",
            nativeQuery = true
    )
    void deleteArticleFromAction(Long serialNumber);

    @Transactional
    @Query(
            value = "SELECT * FROM article",
            nativeQuery = true
    )
    List<Article> getAll();
}
