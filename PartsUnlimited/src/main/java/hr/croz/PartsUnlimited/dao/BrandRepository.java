package hr.croz.PartsUnlimited.dao;

import hr.croz.PartsUnlimited.domain.Brand;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BrandRepository extends JpaRepository<Brand, Long> {

    Brand findByBrandName(String name);

}
