package hr.croz.PartsUnlimited.dao;

import hr.croz.PartsUnlimited.domain.Car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CarRepository extends JpaRepository<Car, Long> {

    Car findByCarName(String name);

}
