package hr.croz.PartsUnlimited.dao;

import hr.croz.PartsUnlimited.domain.Part;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;

@Repository
public interface PartRepository extends JpaRepository<Part, Long> {

    Part findBySerialNumber(Long serialNumber);

    Part findByDateOfManufacture(LocalDate date);

}
