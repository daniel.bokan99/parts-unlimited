package hr.croz.PartsUnlimited.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Article {

    @Id
    private Long serialNumber;

    private Double basePrice;

    @ManyToOne
    @JsonBackReference
    private Action action;

    public Article() { }

    public Article(Long serialNumber, Double basePrice) {
        this.serialNumber = serialNumber;
        this.basePrice = basePrice;
    }

    public Long getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(Long serialNumber) {
        this.serialNumber = serialNumber;
    }

    public Double getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(Double basePrice) {
        this.basePrice = basePrice;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }
}
