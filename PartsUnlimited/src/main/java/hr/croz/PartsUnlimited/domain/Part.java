package hr.croz.PartsUnlimited.domain;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Date;
import java.util.Set;

@Entity(name="Part")
public class Part {

    @Id
    private long serialNumber;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate dateOfManufacture;

    private String partName;

    @ManyToMany(fetch = FetchType.EAGER)
    @JsonIgnore
    private Set<Car> cars;

    public Part(){}

    public Part(long serialNumber, LocalDate dateOfManufacture, String partName) {
        this.serialNumber = serialNumber;
        this.dateOfManufacture = dateOfManufacture;
        this.partName = partName;
    }

    public long getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(long serialNumber) {
        this.serialNumber = serialNumber;
    }

    public LocalDate getDateOfManufacture() {
        return dateOfManufacture;
    }

    public void setDateOfManufacture(LocalDate dateOfManufacture) {
        this.dateOfManufacture = dateOfManufacture;
    }

    public String getPartName() {
        return partName;
    }

    public void setPartName(String partName) {
        this.partName = partName;
    }

    public Set<Car> getCars() {
        return cars;
    }

    public void setCars(Set<Car> cars) {
        this.cars = cars;
    }
}
