package hr.croz.PartsUnlimited.rest;

import hr.croz.PartsUnlimited.dao.ActionRepository;
import hr.croz.PartsUnlimited.domain.Action;
import hr.croz.PartsUnlimited.service.ActionService;
import hr.croz.PartsUnlimited.service.ArticleService;
import hr.croz.PartsUnlimited.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/action")
public class ActionController {

    @Autowired
    private ActionService actionService;

    @Autowired
    private ArticleService articleService;

    @PostMapping("")
    public ResponseEntity<?> addAction(@RequestHeader(value="Authentication") String username, @RequestHeader(value="Authorization") String password,
                                       @RequestBody Action newAction){
        if(!AuthService.AuthorizeSales(username,password)){
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Wrong username or password.");
        }

        if(newAction.getDiscountPercentage() < 0 || newAction.getDiscountPercentage() > 1){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid discount percentage. Percentage must be between 0 and 1");
        }

        Action action = actionService.addAction(newAction);
        return ResponseEntity.ok(action);
    }

    @PostMapping("/addArticleToAction")
    public ResponseEntity<?> addArticleToAction(@RequestHeader(value="Authentication") String username, @RequestHeader(value="Authorization") String password,
                                                @RequestParam(name="articleSerialNumber")Long articleSerialNumber, @RequestParam(name="actionID") Long actionID){

        if(!AuthService.AuthorizeSales(username,password)){
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Wrong username or password.");
        }

        Action action = actionService.addArticleToAction(actionID, articleSerialNumber);
        action.getArticles().add(articleService.findBySerialNumber(articleSerialNumber));
        return ResponseEntity.ok(action);
    }

}
