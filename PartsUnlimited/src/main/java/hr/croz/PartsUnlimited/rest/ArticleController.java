package hr.croz.PartsUnlimited.rest;

import hr.croz.PartsUnlimited.DTO.ArticleDTO;
import hr.croz.PartsUnlimited.domain.Article;
import hr.croz.PartsUnlimited.service.ArticleService;
import hr.croz.PartsUnlimited.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/article")
public class ArticleController {

    @Autowired
    private ArticleService articleService;

    @PostMapping("")
    public ResponseEntity<?> addArticle(@RequestHeader(value="Authentication") String username, @RequestHeader(value="Authorization") String password, @RequestBody ArticleDTO newArticle){

        if(!AuthService.AuthorizeSales(username,password)){
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Wrong username or password.");
        }

        Article article = new Article();
        article.setSerialNumber(newArticle.getSerialNumber());
        article.setBasePrice(newArticle.getBasePrice());

        return ResponseEntity.ok(articleService.addArticle(article));
    }

    @PatchMapping("")
    public ResponseEntity<?> updateBasePrice(@RequestHeader(value="Authentication") String username, @RequestHeader(value="Authorization") String password,
                                             @RequestParam(name = "newPrice") Long newPrice, @RequestParam(name = "serialNumber") Long serialNumber){
        if(!AuthService.AuthorizeSales(username,password)){
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Wrong username or password.");
        }

        return ResponseEntity.ok(articleService.updateBasePrice(serialNumber, newPrice));
    }

    @DeleteMapping("/{serialNumber}")
    public ResponseEntity<?> deleteArticle(@RequestHeader(value="Authentication") String username, @RequestHeader(value="Authorization") String password,
                                           @PathVariable(name="serialNumber") Long serialNumber){

        if(!AuthService.AuthorizeSales(username,password)){
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Wrong username or password.");
        }

        return ResponseEntity.ok(articleService.deleteArticle(serialNumber));
    }

}
