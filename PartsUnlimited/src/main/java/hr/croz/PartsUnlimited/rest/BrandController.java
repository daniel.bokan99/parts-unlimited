package hr.croz.PartsUnlimited.rest;

import hr.croz.PartsUnlimited.DTO.BrandDTO;
import hr.croz.PartsUnlimited.domain.Brand;
import hr.croz.PartsUnlimited.service.AuthService;
import hr.croz.PartsUnlimited.service.BrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/brand")
public class BrandController {

    @Autowired
    private BrandService brandService;

    @PostMapping("")
    public ResponseEntity<?> addBrand(@RequestHeader(value="Authentication") String username, @RequestHeader(value="Authorization") String password,@RequestBody BrandDTO brandDTO){

        if(!AuthService.AuthorizeWarehouse(username,password)){
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Wrong username or password.");
        }

        Brand brand = new Brand();
        brand.setBrandName(brandDTO.getBrandName());

        return ResponseEntity.ok(brandService.addBrand(brand));
    }

}
