package hr.croz.PartsUnlimited.rest;

import hr.croz.PartsUnlimited.DTO.CarDTO;
import hr.croz.PartsUnlimited.domain.Brand;
import hr.croz.PartsUnlimited.domain.Car;
import hr.croz.PartsUnlimited.service.AuthService;
import hr.croz.PartsUnlimited.service.BrandService;
import hr.croz.PartsUnlimited.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/car")
public class CarController {

    @Autowired
    private CarService carService;

    @Autowired
    private BrandService brandService;

    @GetMapping("")
    public ResponseEntity<?> listCars(@RequestHeader(value="Authentication") String username, @RequestHeader(value="Authorization") String password){
        if(!AuthService.AuthorizeWarehouse(username,password)){
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Wrong username or password.");
        }
        return ResponseEntity.ok(carService.listAll());
    }

    @PostMapping("")
    public ResponseEntity<?> addCar(@RequestHeader(value="Authentication") String username, @RequestHeader(value="Authorization") String password, @RequestBody CarDTO carDTO){

        if(!AuthService.AuthorizeWarehouse(username,password)){
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Wrong username or password.");
        }

        Car car = new Car();
        car.setCarName(carDTO.getCarName());
        Brand brand = brandService.findById(carDTO.getBrandFK()).get();
        car.setBrand(brand);
        brand.getCars().add(car);

        return ResponseEntity.ok(carService.addCar(car));
    }

    @GetMapping("partToCar")
    public ResponseEntity<?> addPartToCar(@RequestHeader(value="Authentication") String username, @RequestHeader(value="Authorization") String password, @RequestParam("serialNumber") Long serialNumber, @RequestParam("carID") Long carID){

        if(!AuthService.AuthorizeWarehouse(username,password)){
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Wrong username or password.");
        }

        boolean added = carService.addPartToCar(serialNumber,carID);
        if(added){
            return ResponseEntity.ok("Part " + serialNumber + " successfully added to a car with id " + carID + ".");
        }else{
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Error while trying to add part to a car");
        }
    }


}
