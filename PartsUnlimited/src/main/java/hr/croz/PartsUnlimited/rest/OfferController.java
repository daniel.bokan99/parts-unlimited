package hr.croz.PartsUnlimited.rest;

import hr.croz.PartsUnlimited.service.AuthService;
import hr.croz.PartsUnlimited.service.OfferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/offer")
public class OfferController {

    @Autowired
    private OfferService offerService;

    @GetMapping("")
    public ResponseEntity<?> getCompleteOffer(@RequestHeader(value="Authentication") String username, @RequestHeader(value="Authorization") String password){

        if(!AuthService.AuthorizeCustomer(username,password)){
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Wrong username or password.");
        }

        return ResponseEntity.ok(offerService.getCompleteOffer());
    }

}
