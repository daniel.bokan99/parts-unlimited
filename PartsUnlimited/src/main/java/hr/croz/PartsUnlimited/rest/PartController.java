package hr.croz.PartsUnlimited.rest;

import hr.croz.PartsUnlimited.DTO.DateDTO;
import hr.croz.PartsUnlimited.DTO.PartCountDTO;
import hr.croz.PartsUnlimited.DTO.PartDTO;
import hr.croz.PartsUnlimited.domain.Brand;
import hr.croz.PartsUnlimited.domain.Car;
import hr.croz.PartsUnlimited.domain.Part;
import hr.croz.PartsUnlimited.service.AuthService;
import hr.croz.PartsUnlimited.service.BrandService;
import hr.croz.PartsUnlimited.service.CarService;
import hr.croz.PartsUnlimited.service.PartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api/part")
public class PartController {

    @Autowired
    private PartService partService;

    @Autowired
    private CarService carService;

    @Autowired
    private BrandService brandService;

    @PostMapping("")
    public ResponseEntity<?> addPart(@RequestHeader(value="Authentication") String username, @RequestHeader(value="Authorization") String password, @RequestBody PartDTO part){

        if(!AuthService.AuthorizeWarehouse(username,password)){
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Wrong username or password.");
        }

        Part newPart = new Part();
        newPart.setSerialNumber(part.getSerialNumber());
        newPart.setPartName(part.getPartName());
        newPart.setDateOfManufacture(part.getDateOfManufacture());
        return ResponseEntity.ok(partService.addPart(newPart));
    }

    @DeleteMapping("/{serialNumber}")
    public ResponseEntity<?> deletePart(@RequestHeader(value="Authentication") String username, @RequestHeader(value="Authorization") String password, @PathVariable("serialNumber") Long serialNumber){
        if(!AuthService.AuthorizeWarehouse(username,password)){
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Wrong username or password.");
        }
        return ResponseEntity.ok(partService.removePart(serialNumber));
    }

    @GetMapping("/{serialNumber}")
    public ResponseEntity<?> getPartBySerialNumber(@RequestHeader(value="Authentication") String username, @RequestHeader(value="Authorization") String password, @PathVariable("serialNumber") Long serialNumber){
        if(!AuthService.AuthorizeWarehouse(username,password)){
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Wrong username or password.");
        }

        return ResponseEntity.ok(partService.findBySerialNumber(serialNumber));
    }

    @PostMapping("/date")
    public ResponseEntity<?> getPartByDate(@RequestHeader(value="Authentication") String username, @RequestHeader(value="Authorization") String password, @RequestBody DateDTO dateOfManufacture){
        if(!AuthService.AuthorizeWarehouse(username,password)){
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Wrong username or password.");
        }

        LocalDate date = LocalDate.parse(dateOfManufacture.getDateOfManufacture());
        return ResponseEntity.ok(partService.findByDateOfManufacture(date));
    }

    @GetMapping("/carToPart")
    public ResponseEntity<?> addCarToPart(@RequestHeader(value="Authentication") String username, @RequestHeader(value="Authorization") String password,
                                          @RequestParam(name = "serialNumber") Long serialNumber, @RequestParam(name = "carID") Long carID){

        if(!AuthService.AuthorizeWarehouse(username,password)){
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Wrong username or password.");
        }

        boolean added = partService.addCarToPart(serialNumber,carID);
        if(added){
            return ResponseEntity.ok("Part " + serialNumber + " successfully added to be a part for car with id " + carID + ".");
        }else{
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Error while trying to add car to a part");
        }
    }

    @GetMapping("/brandAndCar")
    public ResponseEntity<?> getPartsByBrandAndCar(@RequestHeader(value="Authentication") String username, @RequestHeader(value="Authorization") String password,
                                                   @RequestParam(name = "param") String param){

        if(!AuthService.AuthorizeWarehouse(username,password)){
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Wrong username or password.");
        }

        String brandName = param.split("\\s+")[0].trim();
        Brand brand = brandService.findByBrandName(brandName);

        String carName = param.split("\\s+")[1].trim();
        Car car = carService.findByCarName(carName);

        if(car == null){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Car with name " + carName + " not found.");
        }else if(brand == null){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Brand with name " + brandName + " not found.");
        }else{
            if(car.getBrand().getBrandName().compareTo(brand.getBrandName()) != 0){
                return ResponseEntity.badRequest().body("Param does not exist");
            }else{
                Set<Part> parts = car.getParts();
                return ResponseEntity.ok(parts);
            }
        }
    }

    @GetMapping("/count")
    public ResponseEntity<?> getCountOfPartsByBrandAndCarName(@RequestHeader(value="Authentication") String username, @RequestHeader(value="Authorization") String password){

        if(!AuthService.AuthorizeWarehouse(username,password)){
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Wrong username or password.");
        }

        List<Car> cars = carService.listAll();
        List<PartCountDTO> resultList = new ArrayList<>();
        for(Car c : cars) {
            String name = c.getBrand().getBrandName() + " " + c.getCarName();
            int count = c.getParts().size();
            resultList.add(new PartCountDTO(name, count));
        }
        return ResponseEntity.ok(resultList);
    }
}
