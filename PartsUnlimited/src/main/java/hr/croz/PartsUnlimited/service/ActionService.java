package hr.croz.PartsUnlimited.service;

import hr.croz.PartsUnlimited.domain.Action;

public interface ActionService {

    public Action addAction(Action action);

    public Action findByActionId(Long id);

    public Action addArticleToAction(Long actionID, Long articleSerialNumber);
}
