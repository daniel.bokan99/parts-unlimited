package hr.croz.PartsUnlimited.service;

import hr.croz.PartsUnlimited.domain.Article;

import java.util.List;

public interface ArticleService {

    public Article addArticle(Article article);

    public Article findBySerialNumber(Long serialNumber);

    public Article updateBasePrice(Long serialNumber, Long basePrice);

    public Article deleteArticle(Long serialNumber);

    public List<Article> getAll();

}
