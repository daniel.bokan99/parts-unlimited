package hr.croz.PartsUnlimited.service;

import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
public class AuthService {

    public static HashMap<String, String> auth;

    static{
        auth = new HashMap<>();
        auth.put("sales", "prodaja");
        auth.put("warehouse", "skladiste");
        auth.put("customer", "kupac");
    }

    public static boolean AuthorizeSales(String username, String password){
        if(username.equals("sales")){
           if(auth.get("sales").equals(password)){
               return true;
           }
        }
        return false;
    }

    public static boolean AuthorizeWarehouse(String username, String password){
        if(username.equals("warehouse")){
            if(auth.get("warehouse").equals(password)){
                return true;
            }
        }
        return false;
    }

    public static boolean AuthorizeCustomer(String username, String password){
        if(username.equals("customer")){
            if(auth.get("customer").equals(password)){
                return true;
            }
        }
        return false;
    }



}
