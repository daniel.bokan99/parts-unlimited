package hr.croz.PartsUnlimited.service;

import hr.croz.PartsUnlimited.domain.Brand;

import java.util.Optional;

public interface BrandService {

    public Brand addBrand(Brand brand);

    public Optional<Brand> findById(Long id);

    public Brand findByBrandName(String name);

}
