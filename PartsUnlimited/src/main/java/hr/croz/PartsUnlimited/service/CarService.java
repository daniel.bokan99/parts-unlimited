package hr.croz.PartsUnlimited.service;

import hr.croz.PartsUnlimited.domain.Car;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface CarService {
    public List<Car> listAll();

    public Car addCar(Car car);

    public Car findById(Long id);

    public boolean addPartToCar(Long serialNumber, Long carID);

    public Car findByCarName(String name);

}
