package hr.croz.PartsUnlimited.service;

import hr.croz.PartsUnlimited.DTO.OfferDTO;
import hr.croz.PartsUnlimited.domain.Article;

import java.util.List;

public interface OfferService {

    List<OfferDTO> getCompleteOffer();

}
