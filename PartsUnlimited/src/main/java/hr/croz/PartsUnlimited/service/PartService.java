package hr.croz.PartsUnlimited.service;

import hr.croz.PartsUnlimited.domain.Part;

import java.time.LocalDate;

public interface PartService {

    public Part addPart(Part part);

    public Part findBySerialNumber(Long serialNumber);

    public Part removePart(Long serialNumber);

    public Part findByDateOfManufacture(LocalDate date);

    public boolean addCarToPart(Long serialNumber, Long carID);

}
