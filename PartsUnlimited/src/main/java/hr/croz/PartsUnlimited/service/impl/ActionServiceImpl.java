package hr.croz.PartsUnlimited.service.impl;

import hr.croz.PartsUnlimited.dao.ActionRepository;
import hr.croz.PartsUnlimited.domain.Action;
import hr.croz.PartsUnlimited.service.ActionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ActionServiceImpl implements ActionService {

    @Autowired
    private ActionRepository actionRepository;

    @Override
    public Action addAction(Action action) {
        long id = (long) actionRepository.addAction(action.getDiscountPercentage(), action.getEndDate(), action.getStartDate());
        return findByActionId(id);
    }

    @Override
    public Action findByActionId(Long id) {
        return actionRepository.findByActionId(id);
    }

    @Override
    public Action addArticleToAction(Long actionID, Long articleSerialNumber) {
        actionRepository.addArticleToAction(actionID, articleSerialNumber);
        actionRepository.addActionToArticle(actionID, articleSerialNumber);
        return findByActionId(actionID);
    }
}
