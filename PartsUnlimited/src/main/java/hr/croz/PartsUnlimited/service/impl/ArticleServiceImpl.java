package hr.croz.PartsUnlimited.service.impl;

import hr.croz.PartsUnlimited.dao.ArticleRepository;
import hr.croz.PartsUnlimited.domain.Article;
import hr.croz.PartsUnlimited.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ArticleServiceImpl implements ArticleService {

    @Autowired
    private ArticleRepository articleRepository;

    @Override
    public Article addArticle(Article article) {
        articleRepository.addArticle(article.getSerialNumber(), article.getBasePrice());
        return findBySerialNumber(article.getSerialNumber());
    }

    @Override
    public Article findBySerialNumber(Long serialNumber) {
        return articleRepository.findBySerialNumber(serialNumber);
    }

    @Override
    public Article updateBasePrice(Long serialNumber, Long basePrice) {
        articleRepository.updateBasePrice(serialNumber,basePrice);
        return articleRepository.findBySerialNumber(serialNumber);
    }

    @Override
    public Article deleteArticle(Long serialNumber) {
        Article article = findBySerialNumber(serialNumber);
        articleRepository.deleteArticleFromAction(serialNumber);
        articleRepository.deleteArticle(serialNumber);
        return article;
    }

    @Override
    public List<Article> getAll() {
        return articleRepository.getAll();
    }
}
