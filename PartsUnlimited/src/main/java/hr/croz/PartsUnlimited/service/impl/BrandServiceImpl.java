package hr.croz.PartsUnlimited.service.impl;

import hr.croz.PartsUnlimited.dao.BrandRepository;
import hr.croz.PartsUnlimited.domain.Brand;
import hr.croz.PartsUnlimited.service.BrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class BrandServiceImpl implements BrandService {

    @Autowired
    private BrandRepository brandRepository;

    @Override
    public Brand addBrand(Brand brand) {
        return brandRepository.save(brand);
    }

    @Override
    public Optional<Brand> findById(Long id) {
        return brandRepository.findById(id);
    }

    @Override
    public Brand findByBrandName(String name) {
        return brandRepository.findByBrandName(name);
    }


}
