package hr.croz.PartsUnlimited.service.impl;

import hr.croz.PartsUnlimited.dao.CarRepository;
import hr.croz.PartsUnlimited.dao.PartRepository;
import hr.croz.PartsUnlimited.domain.Car;
import hr.croz.PartsUnlimited.domain.Part;
import hr.croz.PartsUnlimited.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CarServiceImpl implements CarService {

    @Autowired
    private CarRepository carRepo;

    @Autowired
    private PartRepository partRepository;

    @Override
    public List<Car> listAll() {
        return carRepo.findAll();
    }

    @Override
    public Car addCar(Car car) {
        return carRepo.save(car);
    }

    @Override
    public Car findById(Long id) {
        return carRepo.findById(id).get();
    }

    @Override
    public boolean addPartToCar(Long serialNumber, Long carID) {
        Part part = partRepository.findBySerialNumber(serialNumber);
        Car car = carRepo.findById(carID).get();

        boolean added = car.getParts().add(part);
        carRepo.save(car);

        return added;
    }

    @Override
    public Car findByCarName(String name) {
        return carRepo.findByCarName(name);
    }
}
