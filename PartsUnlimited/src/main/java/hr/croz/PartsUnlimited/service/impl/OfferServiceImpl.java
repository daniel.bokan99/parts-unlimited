package hr.croz.PartsUnlimited.service.impl;

import hr.croz.PartsUnlimited.DTO.OfferDTO;
import hr.croz.PartsUnlimited.domain.Article;
import hr.croz.PartsUnlimited.service.ArticleService;
import hr.croz.PartsUnlimited.service.OfferService;
import hr.croz.PartsUnlimited.service.PartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class OfferServiceImpl implements OfferService {

    @Autowired
    private ArticleService articleService;

    @Autowired
    private PartService partService;

    @Override
    public List<OfferDTO> getCompleteOffer() {

        List<OfferDTO> offer = new ArrayList<>();
        List<Article> articles = articleService.getAll();

        LocalDate todaysDate = LocalDate.now();

        for(Article a : articles){
            OfferDTO o = new OfferDTO();
            o.setSerialNumber(a.getSerialNumber());
            o.setDateOfManufacture(partService.findBySerialNumber(a.getSerialNumber()).getDateOfManufacture());

            if(a.getAction() != null && todaysDate.isBefore(a.getAction().getEndDate())){
                double finalPrice = a.getBasePrice() + a.getBasePrice() * a.getAction().getDiscountPercentage();
                o.setFinalPrice(finalPrice);
            }else{
                o.setFinalPrice(a.getBasePrice());
            }

            offer.add(o);
        }

        return offer;
    }
}
