package hr.croz.PartsUnlimited.service.impl;

import hr.croz.PartsUnlimited.dao.CarRepository;
import hr.croz.PartsUnlimited.dao.PartRepository;
import hr.croz.PartsUnlimited.domain.Car;
import hr.croz.PartsUnlimited.domain.Part;
import hr.croz.PartsUnlimited.service.PartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class PartServiceImpl implements PartService {

    @Autowired
    private PartRepository partRepository;

    @Autowired
    private CarRepository carRepository;

    @Override
    public Part addPart(Part part) {
        return partRepository.save(part);
    }

    @Override
    public Part findBySerialNumber(Long serialNumber) {
        return partRepository.findBySerialNumber(serialNumber);
    }

    @Override
    public Part removePart(Long serialNumber) {
        Part part = findBySerialNumber(serialNumber);

        List<Car> allCars = carRepository.findAll();
        for(Car c : allCars){
            if(c.getParts().contains(part)){
                c.getParts().remove(part);
                carRepository.save(c);
            }
        }

        partRepository.delete(part);
        return part;
    }

    @Override
    public Part findByDateOfManufacture(LocalDate date) {
        return partRepository.findByDateOfManufacture(date);
    }

    @Override
    public boolean addCarToPart(Long serialNumber, Long carID) {
        Part part = partRepository.findBySerialNumber(serialNumber);
        Car car = carRepository.findById(carID).get();
        boolean added = part.getCars().add(car);
        partRepository.save(part);
        return added;
    }
}
